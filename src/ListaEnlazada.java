public class ListaEnlazada {

    Nodo cabeza;
    public ListaEnlazada(){
        this.cabeza = null;
    }

    public void agregar(int dato){
        Nodo nuevoNodo = new Nodo(dato);
        if(cabeza == null){
            cabeza = nuevoNodo;
        }else {
            Nodo actual  = cabeza;
            while(actual.siguiente != null){
                actual =  actual.siguiente;
            }
            actual.siguiente =  nuevoNodo;
        }
    }

    public void eliminar(int dato){
        if(cabeza == null){
            return;
        }

        if (cabeza.dato == dato){
            cabeza = cabeza.siguiente;
            return;
        }

        Nodo actual = cabeza;

        while (actual.siguiente != null && actual.siguiente.dato !=dato){
            actual =  actual.siguiente;
        }

        if (actual.siguiente != null){
            actual.siguiente =  actual.siguiente.siguiente;
        }
    }

    public void imprimir(){
        Nodo actual = cabeza;
        while (actual != null){
            System.out.print(actual.dato + " -> ");
            actual = actual.siguiente;
        }
        System.out.println("null");
    }

    public static void main(String[] args){
        ListaEnlazada lista =  new ListaEnlazada();
        lista.agregar(1);
        lista.agregar(2);
        lista.agregar(3);
        lista.agregar(4);
        lista.agregar(5);
        lista.agregar(6);
        lista.agregar(78);


        System.out.println("Lista Inicial");
        lista.imprimir();

        System.out.println("Lista Despues de Borrar Elemento");
        lista.eliminar(5);
        lista.imprimir();

    }
}
